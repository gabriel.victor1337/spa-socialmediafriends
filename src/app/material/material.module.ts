import { NgModule } from '@angular/core';
import {MatButtonModule, MatCardModule, MatIconModule} from '@angular/material';



@NgModule({
imports: [MatCardModule,MatButtonModule,MatIconModule ],
exports: [MatCardModule,MatButtonModule,MatIconModule ]
})
export class MaterialModule {}