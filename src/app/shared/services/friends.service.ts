import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Friend } from '../model/friend'
import { retry, catchError } from 'rxjs/operators';
@Injectable({
providedIn: 'root'
})
export class FriendsRestApi {
// Define API
apiURL = 'https://reqres.in/api/users?per_page=12';
constructor(private http: HttpClient) { }


// Http Options
httpOptions = {
headers: new HttpHeaders({
'Content-Type': 'application/json'
})
}  
// HttpClient API get() method => Fetch friends list
getFriends(): Observable<Friend> {
return this.http.get<Friend>(this.apiURL)
.pipe(
retry(1),
catchError(this.handleError)
)
}
// Error handling 
handleError(error) {
let errorMessage = '';
if(error.error instanceof ErrorEvent) {
// Get client-side error
errorMessage = error.error.message;
} else {
// Get server-side error
errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
}
window.alert(errorMessage);
return throwError(errorMessage);
}
}