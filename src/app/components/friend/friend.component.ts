import { Component, OnInit } from '@angular/core';
import { FriendsRestApi } from 'src/app/shared/services/friends.service';

@Component({
  selector: 'app-friend',
  templateUrl: './friend.component.html',
  styleUrls: ['./friend.component.scss']
})
export class FriendComponent implements OnInit {
  Friends: any = [];
  constructor(public restApi: FriendsRestApi) { }

  ngOnInit() {
    this.loadFriends() 
  }
// Get Friends list
loadFriends() {
  return this.restApi.getFriends().subscribe((data: {}) => {
  this.Friends = data;
  })
  }
}
